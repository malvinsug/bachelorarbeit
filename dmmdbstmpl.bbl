\begin{thebibliography}{1}

%Author's last name, first name. Book title. Additional information. City of publication: Publishing company, publication date. 

\bibitem{outlier in high}
	Aggarwal, C. C. and P. S. Yu. Outlier Detection in High Dimensional Data,ACM SIGMOD Conference, 2001.

\bibitem{iForest Concept}
	Anello, Eugenia. “Anomaly Detection With Isolation Forest.” Https://Betterprogramming.Pub/Anomaly-Detection-with-Isolation-Forest-E41f1f55cc6, 2021, betterprogramming.pub.

\bibitem{KNN_Outlier}
	Angiulli, F. and C. Pizzuti. Fast Outlier Detection in High Dimensional Spaces. European Conferece on Principles of Knowledge Discovery and Data Mining, 2002. 

\bibitem{OPTICS}
	Ankerst, M., M. M. Breunig, H.-P. Kriegel, and J. Sander, OPTICS: ordering points to identify the clustering structure,  In ACM Sigmod Record, vol. 28, no. 2, pp. 49-60. ACM, 1999.

\bibitem{iNNE}
	Bandaragoda, T. R., K. M. Ting, D. Albrecht, F. T. Liu, and J. R. Wells, “Efficient anomaly detection by isolation using nearest neighbour ensemble,” in Proc. IEEE Int. Conf. Data Mining Workshop, pp. 698–705, 2014.
	
\bibitem{Bellman}
	Bellman, R.E., Dynamic Programming, Princeton University Press, Princeton, NJ, USA, 1957.

\bibitem{TPE}
	Bergstra,J. S., R. Bardenet, Y. Bengio, and B. Kégl. Algorithms for hyperparameter optimization. In Advances in Neural Information Processing Systems 25. 2011.

\bibitem{hyperopt}
	Bergstra, J., Yamins, D., Cox, D. D., Making a Science of Model Search: Hyperparameter Optimization in Hundreds of Dimensions for Vision Architectures. To appear in Proc. of the 30th International Conference on Machine Learning (ICML 2013), 2013.
	
\bibitem{LOF}
	Breunig, M. M., H.-P. Kriegel, R. Ng, and J. Sander, LOF: identifying density-based local outliers. In Proceedings of the ACM International Conference on Management of Data (SIGMOD), Dallas, TX, 93–104, 2000.

\bibitem{Cohen's Kappa}
	Cohen,J.A., Coefficient of agreement for nominal scales. Educational  and Psychological Measurement, pages 37–46, 1960.
	
\bibitem{MDS}
	Cox, T. and M. Cox., Multidimensional Scaling, Number 59 in Monographs on Statistics and Applied Probability. Chapman and Hall, 1994.

\bibitem{DBSCAN}
	Ester, M., H. P. Kriegel, J. Sander, and X. Xu, A Density-Based Algorithm for Discovering Clusters in Large Spatial Databases with Noise, In Proceedings of the 2nd International Conference on Knowledge Discovery and Data Mining, Portland, OR, AAAI Press, pp. 226–231, 1996.

\bibitem{Histopathologic}
	Faust, K. X. et al. Visualizing histopathologic deep learning classification and anomaly detection using nonlinear feature space dimensionality reduction. BMC Bioinforma. 19, 1 –15, 2018.

\bibitem{KNN}
	Fix, E. and J. L. Hodges, Discriminatory analysis – nonparametric discrimination: consistency properties. Tech. Rep. Project No. 21-49-004, USAF School of Aviation Medicine, Randolph Field, TX, 1951.

\bibitem{Hawkins}
	Hawkins, D.: “Identification of Outliers”, Chapman and Hall,London, 1980.

\bibitem{PCA_Name}
	Hotelling, H., Analysis of a complex of statistical variables into principal components. Journal of Educational Psychology, 1933.

\bibitem{Traffic}
	Huang, T., Sethu, H., and Kandasamy, N., A new approach to dimensionality reduction for anomaly detection in data traffic. IEEE Transactions on Network and Service Management, 13,651–665. https://doi.org/10.1109/TNSM.2016.2597125, 2016.
	
\bibitem{NP hard}
	Hutter F., H. Hoos, and K. L.-Brown. An efficient approach for assessing hyperparameter importance. In ICML, pages 754–762, 2014.

\bibitem{DOBIN}
	Kandanaarachchi, S. and R. J. Hyndman, Dimension reduction for outlier detection using DOBIN, Journal of Computational and Graphical Statistics (Accepted). 2020.
	
\bibitem{cluto}
	Karypis, G., "CLUTO A Clustering Toolkit," Dept. of Computer Science, University of Minnesota,Tech. Rep. 02-017, 2002, available at http://www.cs.umn.edu/~cluto

\bibitem{HiCS}
	Keller, F., E. Muller, K. Bohm.HiCS: High-Contrast Subspaces forDensity-based Outlier Ranking,IEEE ICDE Conference, 2012.

\bibitem{ABOD}
	Kriegel, H.-P., M. Schubert, and A. Zimek, Angle-based outlier detection in high-dimensional data. In Proceedings of the 14th ACM International Conference on Knowledge Discovery and Data Mining (SIGKDD), Las Vegas, NV, 444–452, 2008.

\bibitem{LandisKoch}
	Landis, R. J. and G. Koch, G. An application of hierarchical kappa-type statistics in the assessment of majority agreement among multiple observers Biometrics 33, 363– 374, 1977.

\bibitem{feature_bagging}
	Lazarevic, A., and V. Kumar. Feature Bagging for Outlier Detection,ACM KDD Conference, 2005.


\bibitem{COPOD}	
	Li, Z., Zhao, Y., Botta, N., Ionescu, C. and Hu, X. COPOD: Copula-Based Outlier Detection. IEEE International Conference on Data Mining (ICDM), 2020.

\bibitem{iForest}
	Liu, F. T., K. M. Ting, and Z.-H. Zhou, ‘‘Isolation forest,’’ in Proc. 8th IEEE Int. Conf. Data Mining, pp. 413–422, 2008.

\bibitem{MCC}
	Matthews,  B.  W., Comparison  of  predicted  and observed   secondary   structure of T4 phage lysozyme.Biochim. Biophys. Acta,405, 442-451, 1975.

\bibitem{UMAP}
	McInnes, L. and J. Healy, UMAP: Uniform Manifold Approximation and Projection for Dimension Reduction, ArXiv e-prints 1802.03426, 2018.

\bibitem{statistical_selection}
	Muller, E., M. Schiffer, and T. Seidl. Statistical Selection of Rele-vant Subspace Projections for Outlier Ranking.ICDE Conference,pp, 434–445, 2011.


\bibitem{Netflix}
	Onderwater, M., Detecting unusual user profiles with outlier detection techniques. M.Sc. thesis, http://tinyurl.com/vu-thesis-onderwater, 2010.

\bibitem{Preservation}
	Onderwater M., Outlier preservation by dimensionality reduction techniques. Int J Data Anal Tech Strateg 7(3):231–252, 2015.

\bibitem{PCA}
	Pearson, K., F.R.S., LIII. On lines and planes of closest fit to systems of points in space, The London Edinburgh, and Dublin Philosophical Magazine and Journal of Science, 2:11, 559-572, 1901.
	
\bibitem{scikit}
	Pedregosa et al.,Scikit-learn: Machine Learning in Python, JMLR 12, pp. 2825-2830, 2011.

\bibitem{dataset}
	Rayana, Shebuti, ODDS Library [http://odds.cs.stonybrook.edu]. Stony Brook, NY: Stony Brook University, Department of Computer Science, 2016.
	
\bibitem{Kernel Trick}
	Reddy,R.R.,Ramadevi,Y., and Sunitha,K.V.N., Anomaly Detection using Feature Selection and SVM Kernel Trick, International Journal of Computer Applications,975,8887, 2015.
	

\bibitem{Modern Information Retrieval}
	Singhal, A.,  Modern information retrieval: A brief overview, Working paper, Google, Inc.,2009.
	
\bibitem{Sklar}
	Sklar, A.,  “Fonctions de repartition an dimensions et leurs marges,” Publ. inst. statist. univ. Paris, vol. 8, pp. 229–231, 1959.
	
\bibitem{Gitlab}
	Sugiri, M.Y., https://gitlab.com/malvinsug/dimensionality-reduction-with-outlier-detection.git , 2021.

\bibitem{COF}
	Tang, J., Z. Chen, A. W.-C. Fu, and D. W.-L. Cheung, Enhancing effectiveness of outlier detections for low density patterns. In Proceedings of the 6th Pacific-Asia Conference on Advances in Knowledge Discovery and Data Mining (PAKDD). Springer-Verlag, 535–548, 2002.

\bibitem{Isomap}
	Tenenbaum, J.B., Silva V. D., and Langford J. C. , A global geometric framework for nonlinear dimensionality reduction. science,290(5500):2319–2323, 2000.

\bibitem{t-SNE}
	van der Maaten, L.J.P. and  G. Hinton, "Visualizing High-Dimensional Data using t-SNE." Journal of Machine Learning Research. 9:2579-2605, 2008.
	
\bibitem{anomatools}
	Vercruyssen, V., Meert, W., Verbruggen, G., Maes, K., Bäumer, R., Davis, J., Semi-Supervised Anomaly Detection with an Application to Water Analytics. IEEE International Conference on Data Mining (ICDM), Singapore. p527--536, 2018.
\bibitem{PINN}
	Vries T. D., S. Chawla, and M. Houle. Finding Local Anomalies in Very High Dimensional Space,ICDM Conference, 2010.
	
\bibitem{PyOD}
	 Zhao, Y., Z. Nasrullah, and Z. Li, “PyOD: A python toolbox for scalableoutlier detection,”Journal of Machine Learning Research, 2019.

\bibitem{PCA-concept}
	“Principal Component Analysis.” Https://astrostatistics.Psu.Edu/, 2009, astrostatistics.psu.edu/su09/lecturenotes/pca.html.

\end{thebibliography}
