\chapter{Introduction}

As we all know, outliers in the data are unavoidable in machine learning problems. They are usually rare, and they are spread among the data distribution. Sometimes, it is helpful to find these outliers because they indicate that other systems might have tempered the majority of the data. According to Hawkins \cite{Hawkins}:

\say{\textit{An outlier is an observation that deviates so much from the other observations as to arouse suspicions that it was generated by a different mechanism.}}

\begin{figure}
	\begin{center}
		\includegraphics[width=8cm]{figures/outlier-concept.png}
	\end{center}
	\caption{An example of outliers in a dataset.}
	\label{fig: outlier concept}
\end{figure}

If we look at Figure \ref{fig: outlier concept}, we can see there are two types of outliers. The dataset is dominated by two different clusters ($C_1$ and $C_2$) and based on Hawkins definition, we can consider 2 outliers (a$_{1}$ and a$_{2}$). Intuitively, the point a$_1$ can be considered as a \textit{global outlier} since it deviates much from $C_1$ and $C_2$. However the point a$_2$ deviates less from $C_2$. We can consider this as a \textit{contextual outlier} or \textit{local outlier}.

Researchers have been developing numerous outlier detection algorithms for both types of outliers. Some algorithms focus more on finding the local outliers, while others focus more on finding global outliers. These algorithms are useful for classification problems in imbalanced datasets.  For instance, if we have a binary classification problem from 10000 data. However, one of the classes is only represented by 25 data. Using an outlier detection algorithm, one can find the general pattern of the data and distinguish the outliers.

The application of outlier detection has been widely used to solve real-world problems. For example, people use this method to detect credit card fraud, detect diseases, or detect churners (customers who will probably stop doing business with the companies). However, applying the outlier detection algorithms in real-world applications may not be sufficient for the aimed result. In \textit{big data}, we usually collect all the data that can be measured and throw them into the \textit{data lake}. There is a high chance that we take too many features (dimensions) into account during the analysis. It is problematic because as the number of dimensions increases, the distance between points becomes meaningless (the curse of dimensionality)\cite{Bellman}. Mathematically,

$$
lim_{d \rightarrow \infty}  \frac{\text{max-dist}(D)}{\text{min-dist}(D)} = 1
$$

where $D$ is the dataset, and $d$ is the number of dimensions. As the number of dimensions increase, the ratio between maximum distance and minimum distance becomes the same. This problem can usually distort the measurement of the algorithms that use the $k$-nearest neighbors concept.

The typical approach to avoid this problem is to apply dimension reduction techniques. For example, PCA\cite{PCA}, Isomap\cite{Isomap}, t-SNE\cite{t-SNE}, MDS \cite{MDS}, and UMAP\cite{UMAP} have been widely used to reduce dimensions because it has the property of preserving important information.

This thesis will study the relationship between dimension reduction techniques (DR-techniques) and unsupervised outlier detection algorithms (OD-algorithms). Specifically, we will see the applicability of PCA, t-SNE, and UMAP towards the proximity-based OD- algorithms (LOF\cite{LOF}, COF\cite{COF}, and kNN\cite{KNN}), probabilistic OD-algorithms (ABOD\cite{ABOD} and COPOD\cite{COPOD}), and outlier ensembles approach (iForest\cite{iForest} and iNNE\cite{iNNE}). Firstly, we will discuss the related works that share a similar approach for increasing the performance of OD-algorithms. We will also see the mathematical concepts of the OD-algorithms, the DR-techniques, and the hyperparameter optimization technique. Then, we will briefly discuss the metrics used to measure the outlier detection performance, such as accuracy, precision, recall, $F_1$-score, Matthew's correlation, and Cohen's Kappa. Furthermore, we will study the applicability of the DR-techniques on mentioned OD-algorithms based on the experiment results. Finally, we will conclude the experiment based on the applicability of the observed DR-techniques. 

