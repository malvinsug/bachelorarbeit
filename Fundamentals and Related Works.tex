\chapter{Fundamentals and Related Works}

\section{Related Works}
Finding an outlier on the dataset is one of the challenging problems for data mining. Since an outlier is rare, it is difficult to separate the outliers from the dataset. Researchers have been proposing some ideas to detect an algorithm, such as creating measurements for outliers  \cite{LOF}, \cite{COPOD}, or \cite{iForest} or making the outlier detection as the byproduct of the clustering algorithms \cite{OPTICS} \cite{DBSCAN}. However, these methods are not made explicitly for dealing the high-dimensional data. This problem is firstly addressed in \cite{outlier in high}. There has been some research on optimizing the OD-algorithms on high dimensional data, such as selecting subspaces.\cite{feature_bagging}\cite{HiCS}\cite{statistical_selection} However, using these methods can eliminate the meaningful dimensions. 

Another approach to avoid losing meaningful dimensions is to apply the DR-techniques before performing an outlier detection. There have been some studies to this approach, such as \cite{Histopathologic}, \cite{Traffic}, and \cite{Netflix}, but they are specific to the use cases. Some researchers have also developed specific DR-techniques like \cite{DOBIN} and \cite{PINN}. Another study shows the outlier preservation when applying PCA, MDSE, and t-SNE \cite{Preservation}. However, the study only observes the applicability to one OD-algorithm, and the research lacks the dataset. 
Furthermore, there has been a new well-developed DR-technique called UMAP \cite{UMAP}. UMAP is currently on the rise in the machine learning community because it performs better than t-SNE \cite{t-SNE} in many cases. Lastly, there has not been a study on the applicability of the DR-techniques if the given problem has the blessing of dimensionality. It is also helpful for data miners to know which DR-techniques are worth trying because we might have to apply DR-techniques blindfolded in actual cases due to minimal prior knowledge.

This thesis will study the relationship between some popularly used unsupervised OD-algorithms and three well-known DR-techniques: PCA, t-SNE, and UMAP.  For OD-algorithms, we will use ABOD, LOF, COF, COPOD, Isolation Forest, iNNE, and kNN. We will use metrics such as accuracy, precision, recall, $F_1$-score, Matthew's correlation coefficient, and Cohen's kappa to compare the performances. The metrics will be discussed in Chapter 3. 

\section{The Outlier Detection Algorithms}
\subsection{LOF}
As we previously discussed in Chapter 1, we can derive two types of outlier based on Hawkin's outlier definition. This concept motivates Breunig et al. to propose a measure for outlier detection called \textit{local outlier factor score} (LOF) \cite{LOF}. The algorithm measures the "outlierness" for all points using the local outlier factor score. The local outlier factor score strongly depends on the distance between the points since it uses the reachability distance concept. 

\paragraph{$k$-distance Concept}
Let $k$ be a positive integer. We define the $k$-distance of point $p$ as the distance $d(p,o)$ so that for point $o \in D$:

\begin{itemize}
	\item $d(p,o`) \leq d(p,o)$ for at least $k$ points $o' \in D \backslash \{p\}$
	\item $d(p,o`) < d(p,o)$ for at most $k-1$ points $o' \in D \backslash \{p\}$
\end{itemize}

\paragraph{Reachability Distance}
Let $k$ be a positive integer. The \textit{reachability distance of object p relatively to object o} is

$$
\text{reach-dist}_{k}(p,o)= \text{max}(k\text{-distance}(o), d(p,o))
$$

Let us see Figure \ref{fig: reachability distance}. The black point is point $o$. The reachability distance for red points is represented as $k$-distance$(o)$, and the reachability distance of the green point is the same as the distance from point black to point green. Intuitively, the reachability distance is the same as the distance between 2 points as long as $d(p,o)$ is larger than the $k-distance(o)$. Otherwise, we can consider the distance as $k$-distance$(o)$. 

\begin{figure}
	\begin{center}
		\includegraphics[width=5cm]{figures/reachability-distance.png}
	\end{center}
	\caption{Reachability distance}
	\label{fig: reachability distance}
\end{figure}

\paragraph{Local reachability density} 
Based on the reachability distance, we want to measure the \textit{local reachability density}. Mathematically

$$
lrd_{MinPts}(p) = \frac{1}{\mu_{\text{reach-dist}_p}}
$$

where,

$$
\mu_{\text{reach-dist}_p} = \frac{\sum\limits_{o \in N_{MinPts}(p)} \frac{lrd_{MinPts}(o)}{lrd_{MinPts}(p)}}{|N_{MinPts}(p)|}
$$

\paragraph{LOF score}
From local reachability density, we can measure the LOF score.  Generally, the LOF score is the ratio between the average local reachability density from $k$-nearest neighbors points and the local reachability density of the selected point.

$$
LOF_{k}(A) = \frac{\sum\limits_{o \in N_{MinPts}(A)} lrd_{MinPts}(o)}{|N_{MinPts}(A)|} \times \frac{1}{lrd_{MinPts}(A)}
$$

In practice, if the LOF score is higher than one, it is most likely that the data is an outlier. Otherwise, we could consider the data, not as an outlier.

\subsection{COF}
One of the drawbacks of LOF is that it fails to detect the shifting pattern inside the dataset. Let us see Figure \ref{fig: LOF pitfall}. Intuitively speaking, point $A$ should not be an outlier. However, since $A$ has a low density, the algorithm misclassified the object. This example proves that low density does not always imply that the point is an outlier. According to Tang et al., instead of focusing on low density, "isolativity" should be the primary focus for outlier detection. To overcome this problem, they proposed a measure called \textit{the connectivity-based outlier factor}(COF) \cite{COF}.

The main idea of COF is to measure the connectivity between the data points for each point. The algorithm should theoretically be less sensitive to the changes in the data pattern. 

\begin{figure}
	\begin{center}
		\includegraphics[width=8cm]{figures/LOF_pitfall.png}
	\end{center}
	\caption{The pitfall of LOF.The algorithm misclassifies point $A$ as an outlier, due to its low density. }
	\label{fig: LOF pitfall}
\end{figure}

\paragraph{Nearest Neighbor Concept}
Let $P$,$Q$ be a non-empty set, where $P,Q \subseteq \mathcal{D}$ and  $P \cup Q = \emptyset$. Furthermore let us define the distance between $P$ and $Q$ as $dist(P,Q) = min\{dist(x,y) : x \in P\ and\ y \in Q\}$. For any $q \in Q$, $q$ is the nearest neighbor of $P$ in $Q$, if there is $p \in P$ so that $dist(p,q) = dist(P,Q)$. 


\paragraph{Set Based Nearest Path (SBN-path)}
Let  $G$ be sequence and $G \subseteq \mathcal{D}
$. An SBN-path from $p_1$  on  $G$ is a sequence $\langle p_{1},p_{2},\dots,p_{r}\rangle$ so that for all $1\le i \le r-1, p_{i+1}$ is the nearest neighbor of set $\{p1_{1},\dots,p_{i}\}$ in $\{p_{i+1},\dots,p_{r}\}
$.

\paragraph{Set Based Nearest Trail {SBN-trail}}
Let $s = \langle p_{1}, p_{2}, \dots, p_{r}\rangle$
be an SBN-path. The SBN-trail is
$$
\langle e_{1}, e_{2}, \dots, e_{r-1} \rangle
$$
where for all $1 \le i \le r-1$,
$$
e_{i} = (o_{i},p_{i+1})
$$. 
Furthermore, let $o_{i} \in \{p_{1},\dots,p_{i}\}$  and 
$$
dist(e_{i}) = dist(o_{i},p_{i+1}) = dist (\{p_{1}, \dots, p_{i}\},\{p_{i+1},\dots,p_{r}\})
$$.

It is the building block to build the description cost $\langle dist(e_{1}),\dots,dist(e_{r-1})\rangle$.

\begin{figure}
	\begin{center}
		\includegraphics[width=5cm]{figures/cof-concept.png}
	\end{center}
	\caption{ The concept of measuring COF score. Instead of measuring the density around the points, it measures the chain that can be build among the $k$-nearest neighbors. }
	\label{fig: cof-concept}
\end{figure}

\paragraph{Average Chaining Distance}

Based on the SBN-trail, we want to calculate the average chaining distance. Mathematically,

$$
ac-dist_{G}(p_{1}) = \sum_{i=1}^{r-1} \frac{2(r-i)}{r(r-1)} dist(e_{i})
$$

\paragraph{COF Score}
From the average chaining distance, we can define the COF score. The COF score is the ratio between the average chaining distance of a point and the average of the average chaining distance from k-nearest neighbors. 

$$
COF_{k}(p) = \frac{ac-dist_{N_{k}(p)}(p)}{\frac{\sum_{o \in N_{k}(p)} ac-dist_{N_{k}(o)}(o)}{|N_{k}(p)|}}
$$

\subsection{kNN}
The $k$-nearest neighbor (kNN)\cite{KNN} algorithm is originally a supervised machine learning algorithm for classification or regression problems. The algorithm assumes that the object should be classified based on the majority of its surroundings. However, the $k$-nearest neighbor concept can also be purely applied for detecting the outlier. For each point, the algorithm will measure the distance of its $k$-nearest neighbors and combine them as an outlier score \cite{KNN_Outlier}. Intuitively, the higher the score, the more likely it is that the data is an outlier.

\paragraph{$k$-nearest neighbor distance}
Let $D$ be the dataset with $d$ dimensions and let $p$ be a random point from $D$. We can define the $k$-nearest neighbor of point $p$ as the total number of $k$ closest points from point $p$. Furthermore, for each distance between point $p$ and the k-nearest neighbor points, we define

$$
nn_i (p)
$$

where $i$ represents the $i$-th nearest neighborhood of point $p$ in $D$.


\begin{figure}
	\begin{center}
		\includegraphics[width=5cm]{figures/knn-concept.png}
	\end{center}
	\caption{ The main concept of kNN algorithm as an OD-algorithm. }
	\label{fig: knn-concept}
\end{figure}

\paragraph{Outlier Score based on k-nearest neighbor}
Based on the $k$-nearest neighbors concept, we define the outlier score based on the distances between the point and its $k$-nearest neighbor. Mathematically,

$$
\omega_k (p) = \sum_{i=1}^k d_t (p, nn_i (p))
$$

The algorithm will measure the $\omega_k (p)$  score for all points and return the list of outliers based on the $\omega_k (p)$  score.


\subsection{ABOD}
ABOD\cite{ABOD} is an angle-based OD-algorithm. Instead of using distance as an approach to calculate the outlier score, it measures the variance of the angles between 2 vectors, created from the same point to 2 other points. This measurement will be applied to all points in the dataset to get an overview of which data is the outlier. The advantage of ABOD is that the measurement for outlier scores is more stable than distance-based OD-algorithms.  

\begin{figure}
	\begin{center}
		\includegraphics[width=7cm]{figures/abod_intuition.png}
	\end{center}
	\caption{Intuition for ABOD. The figure is taken from \cite{ABOD}.}
	\label{fig: ABOD intuition}
\end{figure}

\paragraph{General Idea}
The idea of ABOD is to break the curse of dimensionality using another measurement. Let us see Figure \ref{fig: ABOD intuition}. Based on the figure, the vectors created inside the cluster create a wide angle. (See $\alpha$ and$ \beta$). Hence, the variance of the angle tends to be higher compared to the real outlier. Although the points are in the cluster border (which can lead to smaller variance), the variances are still higher than the real outlier.

\paragraph{ABOF Score}
Let $D \subseteq \mathbb{R}^d$ and let $\overrightarrow{A}$, $\overrightarrow{B}$, and $\overrightarrow{C}$  be data points in $D$. The angle-based outlier factor $ABOF(\overrightarrow{A})$ as

\begin{align*}
	ABOF(\overrightarrow{A}) =& VAR_{\overrightarrow{B}, \overrightarrow{C} \in D} \left( \frac{\langle \overline{AB}, \overline{AC} \rangle}{\Vert \overline{AB}\Vert^2 \Vert \overline{AC}\Vert^2} \right) \\
	= & \frac{\sum_{\overrightarrow{B} \in D} \sum_{\overrightarrow{C} \in D} \frac{1}{\Vert \overline{AB} \Vert \cdot \Vert \overline{AC}\Vert} \left( \frac{\langle \overline{AB}, \overline{AC} \rangle}{\Vert \overline{AB}\Vert^2 \Vert \overline{AC}\Vert^2} \right)^2}{\sum_{\overrightarrow{B} \in D} \sum_{\overrightarrow{C} \in D} \frac{1}{\Vert \overline{AB} \Vert \cdot \Vert \overline{AC}\Vert}} - \\
	&\left( \frac{\sum_{\overrightarrow{B} \in D} \sum_{\overrightarrow{C} \in D} \frac{1}{\Vert \overline{AB} \Vert \cdot \Vert \overline{AC}\Vert} \cdot \frac{\langle \overline{AB}, \overline{AC} \rangle}{\Vert \overline{AB}\Vert^2 \Vert \overline{AC}\Vert^2}}{\sum_{\overrightarrow{B} \in D} \sum_{\overrightarrow{C} \in D} \frac{1}{\Vert \overline{AB} \Vert \cdot \Vert \overline{AC}\Vert}}\right)^2
\end{align*}

Intuitively, ABOF measures the variance of the angles created by $\overrightarrow{A}$ and any other pair points in $D$ weighted by the distance between the points. Since we need to measure all possible angles created by the data points, the time complexity is $O(n^3)$.

\paragraph{Approximate ABOF for FastABOD}
Using ABOF can be time-consuming, and it is less appealing compared to other OD-algorithms. Therefore, Kriegel et al. propose an approximated ABOF using the $k$-nearest neighbor concept. 

\begin{align*}
	approxABOF_{k}(\overrightarrow{A}) =& VAR_{\overrightarrow{B}, \overrightarrow{C} \in \mathcal{N}_k(\overrightarrow{A})} \left( \frac{\langle \overline{AB}, \overline{AC} \rangle}{\Vert \overline{AB}\Vert^2 \Vert \overline{AC}\Vert^2} \right) \\
	= & \frac{\sum_{\overrightarrow{B} \in \mathcal{N}_k(\overrightarrow{A})} \sum_{\overrightarrow{C} \in \mathcal{N}_k(\overrightarrow{A})} \frac{1}{\Vert \overline{AB} \Vert \cdot \Vert \overline{AC}\Vert} \left( \frac{\langle \overline{AB}, \overline{AC} \rangle}{\Vert \overline{AB}\Vert^2 \Vert \overline{AC}\Vert^2} \right)^2}{\sum_{\overrightarrow{B} \in \mathcal{N}_k(\overrightarrow{A})} \sum_{\overrightarrow{C} \in \mathcal{N}_k(\overrightarrow{A})} \frac{1}{\Vert \overline{AB} \Vert \cdot \Vert \overline{AC}\Vert}} - \\
	&\left( \frac{\sum_{\overrightarrow{B} \in \mathcal{N}_k(\overrightarrow{A})} \sum_{\overrightarrow{C} \in \mathcal{N}_k(\overrightarrow{A})} \frac{1}{\Vert \overline{AB} \Vert \cdot \Vert \overline{AC}\Vert} \cdot \frac{\langle \overline{AB}, \overline{AC} \rangle}{\Vert \overline{AB}\Vert^2 \Vert \overline{AC}\Vert^2}}{\sum_{\overrightarrow{B} \in \mathcal{N}_k(\overrightarrow{A})} \sum_{\overrightarrow{C} \in \mathcal{N}_k(\overrightarrow{A})} \frac{1}{\Vert \overline{AB} \Vert \cdot \Vert \overline{AC}\Vert}}\right)^2
\end{align*}

Basically, since the data points that are far away from point $\overrightarrow{A}$ have small weights, $approxABOF$ can neglect those points. As a result, FastABOD has the time complexity $O(n^2+n \cdot k^2)$, and it can be used for datasets with many points. However, the quality of the $approxABOF$ scores strongly depends on the chosen $k$. 

\subsection{COPOD}
Copula-Based Outlier Detection (COPOD) \cite{COPOD} is a parameter-free OD-algorithm.  Intuitively, the algorithm measures the abnormality of the $p$-value of a data point.  With the help of copula models, the algorithm calculates the left- and right-tail probabilities from each feature and assesses the data as an outlier if it deviates from the general structure. Copula models can separate the marginal distributions from the dependencies from multidimensional distributions.


\paragraph{Copula Models}
Given random variables $(X_1, \cdots , X_d)$, there is a copula so that

$$
F(\textbf{x}) = C(F_1(x_1), \cdots , F_d(x_d))
$$

where for each $j \in 1, \cdots, d$, $F_j$ is a marginal distribution of each dimension. It means we can model a joint distribution $F(\textbf{x})$ using the marginal distributions. Sklar Theorem \cite{Sklar} supports the mathematical foundation of the copula model. 

\paragraph{Empirical Copula}
COPOD uses the empirical cumulative distribution functions (ECDFs) to model the copula of a dataset. 
Let $X_{n \times d}$ be the dataset, where $n$ is th number of observation and $d$ is the number of dimensions. For each value in $X$, we denote it  as $X_{j,i}$. For each $j \in 1, \cdots , d$, we define the ECDF as

$$
\hat{F}(x_{j,i}) = \mathbb{P}((-\infty,x_{j,i}]) = \frac{1}{n} \sum_{i=1}^n \mathbb{I}(X_{j,i} \leq x)
$$

We can simplify the ECDFs expression by

$$
F_X(x_{j,i}) = \mathbb{P}(X_j \leq x_{j,i})
$$

\paragraph{Left and Right Tail Probabilities}
Intuitively, we can measure the occurrence of an observation $x_{j,i}$ based on the ECDFs. An outlier should have a lower occurrence compared to other observations. In most cases, the lower occurrence can also be found on the right side of the distribution. Therefore we should calculate not only the left tail probability $F_X(x_{j,i}) = \mathbb{P}(X_j \leq x_{j,i})$, but we also have to calculate the right tail probability. The right tail probability can be defined as 

$$
\mathbb{P}(X \geq x_{j,i}) = 1 - \mathbb{P}(X < x_{j,i}) = 1 -  s\mathbb{P}(X \leq x_{j,i}) = 1 - F_X (x_{j,i})
$$

\begin{figure}
	\begin{center}
		\includegraphics[width=13cm]{figures/copod_concept.png}
	\end{center}
	\caption{Skewness correction from COPOD. The correction technique gives us more insight on the data distribution. The diagram is taken from \cite{COPOD}.}
	\label{fig: copod_concept}
\end{figure}

\paragraph{Skewness Correction}
Consider a toy dataset based on Figure \ref{fig: copod_concept}. The first column represents the outlier based on ground truth for both the training set and test set. The second and the third column represent the outlier based on left and right tail probabilities, respectively. The left tail probability works well, while the right tail probability works poorly because the data is concentrated on the top right side. If we take both left and right tail probabilities (just like in the fourth column in Figure \ref{fig: copod_concept}), the algorithm's performance will perform poorly since the right tail probability fails to detect the general structure of the data. Therefore, Li and et al. take the skewness into account to take the left tail probability or right tail probability for each observation. We define skewness as

$$
b_i = \frac{\frac{1}{n} \sum_{i=1}^n (x_i - \bar{x_i})^3}{\sqrt{\frac{1}{n-1} \sum_{i=1}^n (x_i - \bar{x_i})^2} ^3}
$$

If the dataset is negatively skewed, we should choose the left tail probabilities over the right and vice versa. With the help of $b_i$, we can optimize the algorithm in finding the correct outliers.

\paragraph{Minimize Tail Probabilities}
As we all know, there is a higher chance that unusual phenomena happen when the number of dimensions increases. As the number of dimensions increases, the value from the copula function decreases exponentially. Mathematically, for each $i \in 1,\cdots,n$

\begin{align*}
	\hat{C}(F_1(x_{1,i}), \cdots , F_d(x_{d,i})) = & \frac{1}{n} \sum_{i=1}^n \mathbb{P} (X \leq x_{1,i}, \cdots , X \leq x_{d,i} ) \\
	= & \frac{1}{n} \sum_{i=1}^n \mathbb{P}(X \leq x_{1,i}) \times \cdots \times  \mathbb{P}(X \leq x_{d,i})
\end{align*}

Hence, it is required to use the $\log ()$ function since it has the monotonicity property. Furthermore, we need to use the sum of the negative log function so that negative numbers can be avoided. Formally

\begin{align*}
	p = - \log(\hat{C}(F_1(x_{1,i}), \cdots , F_d(x_{d,i}))) = - \sum_{j=1}^d \log(F_j(x_{j,i}))
\end{align*}

This will be applied to left tail probabilities as well as to right tail probabilities.

\paragraph{Outlier Score}
As mentioned before, we have three different kinds of measurement. We take the log function of the left tail probabilities $p_l$, the right tail probabilities $p_r$ and the skewness correction $p_s$ for each observation. We define the outlier score as 

$$
\text{Outlier Score } O(x_i) = max\{p_l, p_r, p_s \}
$$ 

\subsection{Isolation Forest}
Isolation Forest\cite{iForest} is an isolation-based outlier detector. It is considered one of the ensembled methods as it gathers weak classifiers to act as a robust classifier. The algorithm creates some decision trees and calculates the average outlier scores of the data based on its depth on each tree. Intuitively, we calculate "the effort" that we need to isolate data. If we need more effort to separate the data, i.e., it has a longer path to isolate the data, it is more likely to be an outlier. 

\paragraph{Isolation Tree (iTree)}
Let $T$ be a node of isolation tree. $T$ could be an external node or an internal node which contains a test $q$ and two childs, denoted as $(T_l , T_r)$. The test $q$ contains a value of $p$ from a randomly selected feature from dataset $X = \{x_1 , x_2 , \dots , x_n \}$, where $n$ represents the total number of data points from $X$. For any datapoint $x_i$, where $1 \leq i \leq n$, if the selected value from $x_i$ is less than value p, then it belongs to $T_l$, otherwise it belongs to $T_r$


\begin{figure}
	\begin{center}
		\includegraphics[width=10cm]{figures/iforest-concept.png}
	\end{center}
	\caption{The main concept of Isolation Forest. The algorithm creates a bunch of trees and each tree is partitioned based on certain value on any dimensions. Theoretically, the easier it is to isolate the data (see red square), the more likely it is an outlier. The figure is taken from \cite{iForest Concept}.}
	\label{fig: iforest_concept}
\end{figure}

\paragraph{Path Length}
To measure "the effort" to isolate data, we define a path length $h(x)$as the depth from the root node until it reaches the external node of a datapoint $x$ inside an iTree.

\paragraph{Defining outlier score}
Based on the path length, we can start defining the outlier score. However, iForest is an algorithm based on many trees. We need to integrate the normalization method so that the trees are comparable to the other. Since iTree shares a similar structure with Binary Search Tree (BST), we can derive it from estimating the average path length for unsuccessful searches in BST. The average path length of an unsuccessful BST search is

$$
c(n) = 2H(n-1) - (\frac{2(n-1)}{n})
$$

where $H(i)$ is the harmonic number, and it can be estimated by

$$
\ln (i) + 0.5772156649  \text{   (Euler's constant)}
$$

Using the monotonic property of exponential function, we can derive the outlier score as

$$
s(x,n) = 2^{\frac{-E(h(x))}{c(n)}}
$$ 

where $E(h(x))$ is the expected path length of data $x$ based on the iTrees generated by iForest.

Based on the paper, we can consider the data an outlier if $s$ approaches 1. Furthermore, if the score approaches 0.5 or less, the data can be considered normal.

\subsection{iNNE}
iNNE (Isolation-based anomaly detection using nearest-neighbor ensembles) \cite{iNNE} is another isolation-based OD-algorithm. It is considered an ensemble classifier like the isolation forest algorithm but focuses more on the local anomalies. The algorithm repetitively takes a sample from the whole dataset and creates hyperspheres based on the nearest neighbors concept. The outlier score will be measured based on the hyperspheres' area and the location of the datapoint.

\paragraph{Hypersphere Concept}
Let $D \subset \mathbb{R}^d$ be a dataset and we create a sample $\mathcal{S}$ based on $D$. Furthermore, let $c \in \mathcal{S}$  and $\eta_c  \in \mathcal{S}$, where $\eta_c$ is the nearest neighbor of $c$. We define a hypersphere $B(c) $ in $\mathcal{S}$ as $\{x: \Vert x-c \Vert < \tau (c)\}$, where $x \in \mathbb{R}^d$ and $\tau(c) = \Vert \eta_c - c\Vert$.

\paragraph{Isolation Score}
Instead of measuring globally, we focus more on the local measure of the data. Therefore, we compare the area between $B(c)$ and $B(\eta_c)$. Intuitively, if $\tau(c)$ is large enough compared to $\tau(\eta_c)$, the more it is likely that $c$ is an outlier. Hence, we can define the isolation score as

$$
I(x) = 
\begin{cases}
	1  - \frac{\tau(\eta_{cnn(x)})}{\tau(cnn(x))} & \text{if } x \in \bigcup\limits_{c \in \mathcal{S}} B(c) \\
	1 & \text{otherwise}
\end{cases}
$$

where $cnn(x) =  \argmax_{c \in \mathcal{S}} \{\tau(c) : x \in B(c)\}$. Intuitively, for any data point $x$, we find a point from $\mathcal{S}$ that has the smallest radius $\tau(c)$.

\paragraph{Anomaly Score}
Based on the isolation score, we can derive the anomaly score from the ensembles. The anomaly score would be the average isolation score from each ensemble. Mathematically,

$$
\bar{I}(x) = \frac{1}{t} \sum_{i=1}^{t} I_i (x)
$$

where $t$ is the total number of ensembles used in the algorithm. The illustrative idea of iNNE can be seen in Figure \ref{fig: inne-concept}.

\begin{figure}
	\begin{center}
		\includegraphics[width=7cm]{figures/inne-concept.png}
	\end{center}
	\caption{The illustrative example on how iNNE works. The data points in the figure are the sample based on artificial dataset. The figure is taken from \cite{iNNE}.}
	\label{fig: inne-concept}
\end{figure}

\section{The Dimensionality Reduction Techniques}
\subsection{PCA}
The idea of principal component analysis (PCA) was initially proposed by Pearson\cite{PCA} in 1901 and later developed by Hotelling\cite{PCA_Name} in 1933. The main idea is to reduce dimensions from $d$-dimensional subspace by projecting points into $k$-dimensional subspace ($k<d$). 

\begin{figure}
	\begin{center}
		\includegraphics[width=7cm]{figures/pca_example.png}
	\end{center}
	\caption{This is a dataset of students score in statistic and astrophysics class. With the help of PCA, we can extract the similar pattern with only one dimension. The figure is taken from \cite{PCA-concept}.}
	\label{fig: PCA example}
\end{figure}

Let us see this Figure \ref{fig: PCA example}. If we want to compare the students using one dimension, we should take a linear combination from both axes. To minimize the information loss, we should take the "direction" with the strongest variance. Mathematically, one should calculate the eigenvalues and eigenvectors from the covariance matrix of the data. 

\paragraph{Covariance Matrix}
Let $X$ be the matrix of the data $(n \times d)$ and let $\tilde{X}$ be the centralized form of $X$ ($\mu = 0$). To calculate the covariance matrix,

$$
\Sigma_{X} = \frac{1}{n} \tilde{X} ^T\tilde{X}
$$

\paragraph{Eigenvalues $\lambda$}
To find the possible eigenvalues, one should calculate the characteristic polynomial of $\Sigma_{\tilde{X}}$. Formally,

$$
det(\Sigma_{\tilde{X}} - \lambda I) = 0
$$

The number of possible eigenvalues equals the number of the dimension from the X data. Based on intuition, one should take the $k$-highest eigenvalue to minimize information loss. 

\paragraph{Eigenvectors $v$}
After finding the eigenvalues, one should find the $k$-eigenvectors $v$ that represent the highest eigenvalues. 

$$
(\sigma_{\tilde{X}} - \lambda I)v = 0
$$

\paragraph{Finding reduced dimension}
To find the reduced dimension, one should join the $k$-eigenvectors and form matrix $P$ $(d \times k)$ and multiplied it with $\tilde{X}$.
$$
Y = \tilde{X}P
$$

\subsection{t-SNE}
t-SNE (t-Distributed Stochastic Neighbor Embedding)\cite{t-SNE} is a DR-technique that lies under the non-linearity assumption. Compared to PCA, t-SNE preserves the local structure of the data. The algorithm is based on Kullback-Leibler divergence (KL-divergence) and uses the Student t-distribution to compute the similarity between two data points. In general, the algorithm starts with generating random points in lower dimensions. The algorithm will compare the data distribution between the data in high and lower dimensions through iteration. t-SNE minimizes the difference between them using KL-divergence. To understand the concept behind t-SNE, one should first understand the Stochastic Neighbor Embedding (SNE) concept.

\paragraph{Stochastic Neighbor Embedding} 
Given datapoint $x_i$ and $x_j$, the conditional probability that the datapoint $x_j$ is similar to $x_i$ is

$$
p_{j|i} = \frac{\exp(-\Vert x_{i}-x_{j}\Vert ^2 / 2\sigma_i^2)}{\Sigma_{k \neq i} \exp(-\Vert x_{i}-x_{k}\Vert ^2 / 2\sigma_i^2)}
$$ 

where $\sigma_i$ is the Gaussian variance of point $x_i$.  To determine $\sigma_i$, we introduce the perplexity as a hyperparameter. Mathematically,

$$
Perp(P_{i}) = 2^{H(P_{i})}
$$ 

where $H(P_{i})$ is the Shannon entropy of $P_{i}$.

$$
H(P_{i}) = - \sum_{j} p_{j|i} \log _2 p_{j|i}
$$

Moreover, under lower-dimensional counterparts, we want to define the conditional probabilities that $y_j$ is similar to $y_i$ as follow:

$$
q_{j|i} = \frac{\exp(-\Vert y_{i}-y_{j}\Vert ^2 )}{\Sigma_{k \neq i} \exp(-\Vert y_{i}-y_{k}\Vert ^2 )}
$$

Using Kullback-Leibler divergence, we can measure the difference between $P_i$ and $Q_i$ distribution.

$$
C = \sum_{i} KL(P_{i} \Vert Q_{i}) = \sum_i \sum_j p_{j|i} log \frac{p_{j|i}}{q_{j|i}}
$$


\paragraph{t-SNE}
Although using the SNE concept, we can measure the similarities between the data points well, t-SNE aims to optimize SNE. Instead of calculating the conditional probabilities $p_{j|i}$ and $q_{j|i}$, t-SNE calculates the joint probabilities $p_{ij}$. We define the joint probabilities $p_{ij}$ as

$$
p_{ij} = \frac{p_{j|i} + p_{i|j}}{2n}
$$

For the low dimensional counterpart, we introduce the Student t-distribution with a single degree of freedom. Using Student t-distribution, the data points separated by a large distance become stable during the changes in the map scale due to its heavy-tailed property. So the joint probabilities in the lower dimensional space are defined as

$$
q_{ij} = \frac{(1+\Vert y_i - y_j \Vert^2)^{-1}}{\sum_{k \neq l} (1+\Vert y_k - y_l \Vert^2)^{-1} }
$$

To minimize the KL-divergence between $P$ and $Q$, we compute the gradient as

$$
\frac{\partial C}{\partial y_i} = 4 \sum_j (p_{ij}-q_{ij})(y_i - y_j )(1 + \Vert y_i - y_j \Vert^2)^{-1}
$$

\subsection{UMAP}
Another non-linear DR-technique is UMAP (Uniform Manifold Approximation and Projection for Dimension Reduction)\cite{UMAP}. The main idea is similar to t-SNE; we minimize the difference between the data in the high and low dimensions. However, instead of using Student t-distribution and KL-divergence, UMAP is driven by the topological theory and cross-entropy.

\paragraph{$k$-neighbor graph}
UMAP defines the data distribution using the k-neighbor graph concept. This concept is motivated by a solid mathematical foundation, namely the topological theory. The concept is discussed in Section 2 of the paper. From the computational view, we define the conditional probability that the datapoint $x_i$ is similar to $x_j$ as 

$$
p_{i|j} = \frac{-max (0,d(x_i,x_{i_j}))-\rho_i}{\sigma_i}
$$ 

where

$$
\rho_i = min\{ d(x_i,x_{i_j}) | 1 \leq j \leq k, d(x_i,x_{i_j}) > 0\}
$$ 

and let $\sigma_i$ be the value so that

$$
\sum_{j=1}^k \exp(p_{i|j}) = \log_2(k)
$$

Intuitively, $\rho_i$ is the shortest distance from point $x_i$ to its $k$-nearest neighbor, and $\sigma_i$ is the smoothed normalization factor based on the Riemannian metric.

Now, we can build a weighted directed graph $G = (V,E,w)$ where $V$ represents all the data points in $X$. Moreover, we define $E$ as edges in $G$, where

$$
E = \{(x_i,x_{i_j}) | 1 \leq j \leq k, 1 \leq i \leq N\}
$$			


and 

$$
w((x_i,x_j)) = \exp (p_{i|j})
$$

To measure the probability that at least one of the two directed edges between point $x_i$ and point $x_j$ exists, we will use the weighted adjacency matrix of $G$, and the probabilities will be represented as matrix $B$. Mathematically,

$$
B = A + A^\intercal - A \circ A^\intercal 
$$

where $\circ$ represents the pointwise product, and $A_{ij}$  represents the probability that the directed edge from point $x_i$ to $x_j$ exists. 

Furthermore, we want to represent the data distribution on embedded space. Mathematically, we define it as 

$$
\phi(y_i,y_j) = \left( 1+ a(\Vert y_i - y_j \Vert_2^2)^b \right)^{-1}
$$

where $a$ and $b$ are hyperparameters. $a$ and $b$ are chosen from non-linear least-square fitting to $\phi(y_i,y_j)$ based on min-dist.

$$
\phi(y_i,y_j) = 
\begin{cases}
	1  & \text{if } \Vert y_i - y_j \Vert_2 \leq \text{min-dist} \\
	\exp (-(\Vert y_i - y_j \Vert_2 - \text{min-dist})) & \text{otherwise}
\end{cases}
$$

\paragraph{Cross Entropy}
To measure the information loss, UMAP uses cross-entropy. Based on information theory, given two distributions $P$ and $Q$, the cross-entropy is the expected number of bits to send the data from $P$ distribution using the distribution from $Q$. Mathematically,

$$
H(p,q) = - \sum_{i=1}^n p(x_i) \log_2 q(x_i)
$$

Using this concept, we want to measure the cross-entropy from the embedded space based on raw data. However, UMAP aims for preserving the global structure, therefore 

$$
CE(w,\phi ) = \sum_{i \neq j} w(x_i,x_{i_j}) \log_2 \left( \frac{w(x_i,x_{i_j})}{\phi(x_i,x_j)} \right) + (1-w(x_i,x_{i_j})) \log_2 \left( \frac{(1-w(x_i,x_{i_j}))}{(1-\phi(x_i,x_j))}\right) 
$$

The right part ensures that the data points which are not similar, separate from each other. Hence, UMAP does not only aim for the local structure but also the global structure.

\section{Tree-structured Parzen Estimator Approach (TPE)}
This subsection will briefly discuss how the Tree-structured Parzen Estimator (TPE) \cite{TPE} approach is used for hyperparameter optimization. As we all know, tuning hyperparameter is an NP-hard problem \cite{NP hard} and plays a significant role in machine learning. The hyperparameters affect the performance of the algorithm. There have been some approaches like grid search and random search. Though these approaches work well in some cases, they lack sense in finding the optimal hyperparameter. Therefore, we use the TPE approach, which has a heuristic method to find the optimal hyperparameter.

\paragraph {Hyperparameter Optimization Function} Let $f$ be an objective function that takes $x$ as the hyperparameter. We define an optimal hyperparameter $\hat{x}$ as 

$$
\hat{x} = \argmin_{x \in \mathcal{X}} f(x)
$$

Intuitively, the goal is to find the hyperparameter which minimizes the score of the objective function. We can find the optimal hyperparameter by doing some iteration over time.

\paragraph{Expected Improvement Function}
Initially, the TPE approach is based on a random search. It chooses a couple of random hyperparameters from the given search spaces and measures their objective function. To "predict" the next better hyperparameter for the next iteration, TPE uses an Expected Improvement (EI) criterion function. The function measures the expected value of improvement of the objective function score from all possible hyperparameters.  Formally, let $M$ be a model of $f: \mathcal{X} \leftarrow \mathbb{R}$, the EI function is defined as

$$
EI_{y^*}(x) = \int_{-\infty}^{+\infty} \text{max}(y^* - y,0) p_M (y|x) dy
$$

where $y^*$ is the threshold based on specific quantile and y is the score of the previous hyperparameter.

\paragraph{Bayesian Optimization} 
Based on the EI criterion, we can see that the function is dependent on $p(y|x)$. Using the Bayesian principle, we can build a probability function that indicates where the optimized hyperparameter can be found. Mathematically, we define the Bayesian rule as 

$$
p(y|x) = \frac{p(x|y) p(y)}{p(x)}
$$  

$p(x|y)$ represents the probability of the hyperparameter given the specific score of an objective function. To make a tree structure, we define $p(x|y)$ as

$$
p(x|y) = 
\begin{cases}
	l(x) & \text{if } y < y^* \\
	g(x) & \text{if } y \geq y^*
\end{cases}
$$

Essentially, we divide the histories of the chosen hyperparameters into two different distributions and their score by the threshold $y^*$ The two distributions are denoted as $l(x)$ and $g(x)$ respectively. Intuitively, $l(x)$ is preferred since it yields a lower score for the objective function. This intuition can also be derived from the EI function. The derivation can be found on the original paper. Mathematically,

$$
EI_{y^*} (x) \propto \left( p(y < y^*) + \frac{g(x)}{l(x)} (1- p(y < y^*))\right)^{-1}
$$

The term amplifies that in order to increase the EI score, one should minimize the $\frac{g(x)}{l(x)}$ ratio, hence we should choose $x$ which has low $g(x)$ and high $l(x)$. It will ensure that we will always have the highest EI score.


