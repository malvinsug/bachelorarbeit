\chapter{Experiment}
In the experiment for this thesis, seven previously mentioned unsupervised OD-algorithms will be applied to 15 datasets. Each algorithm will be combined with one of the following: PCA, t-SNE, and UMAP. Using a tree-structured Parzen estimator approach,  the program will continuously try to optimize the model performance. As the benchmark, we will use the optimized model performances from the OD-algorithms without the dimensionality reduction. The method used in the experiment is illustrated in Figure \ref{fig: experiment_pipeline}. 

\begin{figure}[htbp]
	\begin{center}
		\includegraphics[width=12cm]{figures/experiment_diagram.png}
	\end{center}
	\caption{The method in the experiment. Using tree-structured Parzen estimator approach, the hyperparameters of each algorithm will be optimized.}
	\label{fig: experiment_pipeline}
\end{figure}

The datasets for the experiment are taken from ODDS \cite{dataset} and CLUTO \cite{cluto}. The overview of the datasets can be seen in Table \ref{tab:dataset-info}. The datasets from CLUTO are artificial, and we also want to study if DR-techniques can also be applied in two dimensions for optimizing the OD-algorithms. Four datasets are taken from medical data. We observe if the algorithm can detect breast cancer (WBC), diabetes(Pima), pathologic fetal heart rate(Cardio), and disk hernia(Vertebral). We will also apply the algorithms for analyzing localization sites of a protein(Ecoli and Yeast). To extend the experiment, we are also going to use the algorithms to detect outliers in glass dataset (for crime investigation), wine, radar in the ionosphere (Ionosphere), handwriting recognition (Pendigits), and speech analysis (Vowels).

\begin{table} [htbp]
	\resizebox{\textwidth}{!}{ 
		\begin{tabular}{|c|cc|c|cc|}
			\hline
			Dataset & (points,dimensions) & outlier & dataset & (points,dimensions) & outlier\\ 
			\hline
			WBC & (278,30) & 5.6\% & Wine & (129,13) & 7.7\% \\
			Glass & (214,9) & 4.2\% &  Vertebral & (240,6) & 12.5\% \\
			Vowels & (1456,12) & 3.4\% & Yeast & (1364,8) & 4.7\% \\
			Cardio & (1831,21) & 9.6\% & Cluto-t4-8k & (8000,2) & 9.55\% \\
			Pima & (768,8) & 35\% & Cluto-t5-8k & (8000,2) & 14.4125\% \\
			Ionosphere & (351,33) & 36\% &  Cluto-t7-10k & (10000,2) & 7.92\% \\
			Pendigits & (6870,16) & 2.27\% & Cluto-t8-8k & (8000,2) & 4.0375\%\\
			Ecoli & (336,7) & 2.6\% & & & \\
			\hline
		\end{tabular}
	}
	\caption{The overview of the datasets in the experiment.}
	\label{tab:dataset-info}
\end{table}

For the implementation we will use the PyOD\cite{PyOD}  version 0.8.8 library and Anomatools\cite{anomatools} version 2.3.0 library. These libraries will provide the needed OD-algorithms. We will also use the Scikit-learn\cite{scikit} version 0.24.1 library for PCA and t-SNE and Umap-learn \cite{UMAP} version 0.5.1 for UMAP. For the hyperparameter optimization, we will use the Hyperopt\cite{hyperopt} version 0.2.5. The experiment is executed at a MacBook Pro laptop with 2,5 GHz Quad-Core Intel Core i7-4770HQ, and 16 GB memory is used.

\section{Objective Function}

To optimize the performance, we want to achieve a high score based on metrics mentioned in Chapter 3. Ideally, we want to reduce the false negatives and false positives from the confusion matrix. So we will define the objective function $f$ as the negative sum of accuracy, precision, recall, $F_1$-score, MCC, and Cohen's Kappa $\kappa$. We use the negative sign for the objective function because the TPE approach aims to minimize the score of the objective function. 

\begin{figure}[htbp]
	\[f = -(Accuracy + Precision + Recall + F_{1}-score + MCC + \kappa)\]
	\caption{The objective function}
\end{figure}

In practice, we usually use the precision score, the recall score, and the $F_1$-score to measure how well the performance of the OD-algorithm. Ideally, we want to make sure that all these metrics are in agreement and achieve high scores. Since the MCC score and Cohen's kappa score have a property that also takes the data in the negative class into account, according to Chapter 3, we hypothesize that these metrics can act as the penalization method for the objective function. The same principle also applies to the accuracy score. Though precision in most cases is more reliable compared to the accuracy score, theoretically, there is also a chance where a machine learning model has a higher precision but low accuracy. 

\begin{table} [h]
	\centering
	\begin{tabular}{c|l|c|c|c}
		\multicolumn{2}{c}{}&\multicolumn{2}{c}{\textit{Prediction}}&\\
		\cline{3-4}
		\multicolumn{2}{c|}{}&Positive&Negative&\multicolumn{1}{c}{Total}\\
		\cline{2-4}
		\textit{Ground} & Positive & $50$ & $300$ & $350$\\
		\cline{2-4}
		\textit{Truth} & Negative & $0$ & $650$ & $650$\\
		\cline{2-4}
		\multicolumn{1}{c}{} & \multicolumn{1}{c}{Total} & \multicolumn{1}{c}{$50$} & \multicolumn{1}{c}{$950$} & \multicolumn{1}{c}{$1000$}\\
	\end{tabular}
	%FIXME: Needs a better caption
	\caption{An example where the model achieve a higher precision score but lower accuracy score.}
	\label{tab:Accuracy lower}
\end{table}

Based on Table \ref{tab:Accuracy lower}, we can see that the prediction has a precision of $100\%$ but accuracy of $70\%$. By adding the accuracy to the objective function, we can "discourage" the optimization method for choosing this model. The penalization method is used to avoid the overfitting of a model. 

Furthermore, we will denote the objective function result as $f_{D-OD-DR}$ where $D$ is the used dataset, $OD$ is the name of the OD-algorithm used, and $DR$ is the name of the used DR-technique. If the DR-technique is not applied, we will substitute $DR$ with $None$.

Since we do not know if the given dataset suffers from the curse of dimensionality, we will categorize the dataset into two categories. For any $OD$, there should be at least one $DR$, so that 

$$
f_{D-OD-None} < f_{D-OD-DR} \implies D \in \mathcal{D}_{cd} 
$$

and 

$$
f_{D-OD-None} \ge f_{D-OD-DR} \implies D \in \mathcal{D}_{\neg cd}
$$

Intuitively, we assume $\mathcal{D}_cd$ as a set of the dataset that suffers from the curse of dimensionality and $\mathcal{D}_{\neg cd}$ as a set of the dataset that does not suffer from the curse of dimensionality. Based on the conducted experiment, the $\mathcal{D}_cd$ set and $\mathcal{D}_{\neg cd}$ set are listed in Table \ref{tab:cd dataset}.


\begin{table} [htbp]
	\centering
	\begin{tabular}{|c|c|}
		\hline
		$\mathcal{D}_{cd}$ & $\mathcal{D}_{\neg cd}$ \\ 
		\hline
		WBC & Cluto-t4-8k \\
		Glass &Cluto-t5-8k \\
		Vowels & Cluto-t7-10k \\
		Cardio & Cluto-t8-8k \\
		Pima & \\
		Ionosphere & \\
		Pendigits & \\
		Ecoli & \\
		Wine & \\
		Vertebral & \\
		Yeast & \\
		\hline
	\end{tabular}
	\caption{The list of dataset that belongs to $\mathcal{D}_{cd}$ and $\mathcal{D}_{\neg cd}$.}
	\label{tab:cd dataset}
\end{table}

\section{Experiment Result}
In this section, we will see the result of the experiment. For the complete results, the tables are shown in Appendix. As mentioned in Table \ref{tab:cd dataset}, there are 11 dataset that belong to $\mathcal{D}_{cd}$ and 6 dataset from $\mathcal{D}_{\neg cd}$. To compare the performance based on the objective function $f$, we calculate the average $f$ score based on the combination of the OD-algorithms and the DR-techniques. Furthermore, we also calculate the overall score of each metric to see if we can get more insight into performance.

\subsection{Result based on $\mathcal{D}_{cd}$} \label{results in Dcd}

\begin{table} [htbp]
	\resizebox{\textwidth}{!}{ 
		\begin{tabular}{|l|c|ccc|} 
			\hline 
			Detector & Total-None & Total-PCA & Total-t-SNE & Total-UMAP \\ \cline{1-5} 
			ABOD & 2.5008 & \textbf{2.6166} & 1.958 & 2.2263 \\  
			COF & 2.5168 & \textbf{2.5734} & 2.117 & 2.2439 \\  
			COPOD & 2.1954 & 1.9476 & \textbf{2.8096} & 2.7403 \\  
			iForest & 2.5269 & \textbf{2.7196} & 2.4845 & 2.6832 \\  
			iNNE & 2.4833 & \textbf{2.6084} & 1.6132 & 1.638 \\  
			LOF & 2.6147 & 2.7617 & 2.9054 & \textbf{2.9105} \\  
			kNN & 2.8063 & \textbf{2.9333} & 2.9236 & 2.9024 \\ 
			\hline 
			AVERAGE & 2.5206 & \textbf{2.5944} & 2.4016 & 2.4778 \\  
			\hline 
		\end{tabular}% 
	} 
	\caption{Average score based on the objective function.}
	\label{tab:Overall Objective Score}
\end{table}

\begin{table} [htbp]
	\resizebox{\textwidth}{!}{ 
		\begin{tabular}{|l|c|ccc|} 
			\hline 
			Detector & Accuracy-None & Accuracy-PCA & Accuracy-t-SNE & Accuracy-UMAP \\ \cline{1-5} 
			ABOD & 0.8534 & \textbf{0.8567} & 0.836 & 0.8472 \\  
			COF & 0.8545 & \textbf{0.8561} & 0.8413 & 0.8493 \\  
			COPOD & 0.845 & 0.8379 & \textbf{0.8619} & 0.8583 \\  
			iForest & 0.8551 & \textbf{0.8606} & 0.8522 & 0.8576 \\  
			iNNE & 0.8526 & \textbf{0.8561} & 0.826 & 0.8311 \\  
			LOF & 0.8575 & 0.8642 & 0.8636 & \textbf{0.8669} \\  
			kNN & 0.8569 & \textbf{0.8619} & 0.8595 & 0.8544 \\  
			\hline
			AVERAGE & 0.8536 & \textbf{0.8562} & 0.8486 & 0.8521 \\  
			\hline 
		\end{tabular}% 
	}
	\caption{Average score based on the accuracy score $\mathcal{D}_{cd}$.}
	\label{tab:Overall Accuracy Score}
\end{table}

\begin{table} [htbp]
	\resizebox{\textwidth}{!}{ 
		\begin{tabular}{|l|c|ccc|} 
			\hline
			Detector & Precision-None & Precision-PCA & Precision-t-SNE & Precision-UMAP \\ \cline{1-5} 
			ABOD & 0.3454 & \textbf{0.3618} & 0.2584 & 0.3144 \\  
			COF & 0.3512 & \textbf{0.3587} & 0.2848 & 0.3246 \\  
			COPOD & 0.3039 & 0.2688 & \textbf{0.3876} & 0.3694 \\  
			iForest & 0.3544 & \textbf{0.3808} & 0.3381 & 0.3673 \\  
			iNNE & 0.3412 & \textbf{0.3588} & 0.2092 & 0.2341 \\  
			LOF & 0.3656 & 0.3991 & 0.3957 & \textbf{0.4124} \\  
			kNN & 0.3843 & \textbf{0.4069} & 0.3957 & 0.3724 \\  
			\hline
			AVERAGE & 0.3494 & \textbf{0.3621} & 0.3242 & 0.3421 \\  
			\hline
		\end{tabular}% 
	}
	\caption{Average score based on the precision score $\mathcal{D}_{cd}$.}
	\label{tab:Overall Precision Score} 
\end{table}

\begin{table} [htbp]
	\resizebox{\textwidth}{!}{ 
		\begin{tabular}{|l|c|ccc|} 
			\hline 
			Detector & MCC-None & MCC-PCA & MCC-t-SNE & MCC-UMAP \\ \cline{1-5} 
			ABOD & 0.2828 & \textbf{0.3062} & 0.1733 & 0.2294 \\  
			COF & 0.2863 & \textbf{0.2978} & 0.2036 & 0.2362 \\  
			COPOD & 0.2198 & 0.1695 & \textbf{0.3434} & 0.3283 \\  
			iForest & 0.2879 & \textbf{0.3275} & 0.2778 & 0.3172 \\  
			iNNE & 0.2785 & \textbf{0.3039} & 0.1057 & 0.112 \\  
			LOF & 0.3058 & 0.3364 & 0.3631 & \textbf{0.3656} \\  
			kNN & 0.3444 & \textbf{0.3705} & 0.3667 & 0.3601 \\ 
			\hline 
			AVERAGE & 0.2865 & \textbf{0.3017} & 0.2619 & 0.2784 \\  
			\hline 
		\end{tabular}% 
	} 
	\caption{Average score based on the MCC	score $\mathcal{D}_{cd}$.}
	\label{tab:Overall MCC Score} 
\end{table}

\begin{table} [htbp]
	\resizebox{\textwidth}{!}{ 
		\begin{tabular}{|l|c|ccc|} 
			\hline 
			Detector & Recall-None & Recall-PCA & Recall-t-SNE & Recall-UMAP \\ \cline{1-5} 
			ABOD & 0.467 & \textbf{0.4999} & 0.3035 & 0.3708 \\  
			COF & 0.4667 & \textbf{0.4817} & 0.375 & 0.3358 \\  
			COPOD & 0.3822 & 0.3209 & \textbf{0.5366} & 0.5252 \\  
			iForest & 0.4712 & \textbf{0.5273} & 0.4651 & 0.5075 \\  
			iNNE & 0.4609 & \textbf{0.4947} & 0.2042 & 0.1821 \\  
			LOF & 0.4878 & 0.5063 & \textbf{0.5751} & 0.5564 \\  
			kNN & 0.5599 & 0.5848 & 0.5848 & \textbf{0.5952} \\
			\hline 
			AVERAGE & 0.4708 & \textbf{0.4879} & 0.4349 & 0.439 \\  
			\hline 
		\end{tabular}% 
	} 
	\caption{Average score based on the recall score $\mathcal{D}_{cd}$.}
	\label{tab:Overall Recall Score} 
\end{table}

\begin{table} [htbp]
	\resizebox{\textwidth}{!}{ 
		\begin{tabular}{|l|c|ccc|} 
			\hline 
			Detector & F1-Score-None & F1-Score-PCA & F1-Score-t-SNE & F1-Score-UMAP \\ \cline{1-5} 
			ABOD & 0.3082 & \textbf{0.3273} & 0.229 & 0.2658 \\  
			COF & 0.3109 & \textbf{0.321} & 0.2411 & 0.2815 \\  
			COPOD & 0.2562 & 0.211 & \textbf{0.3691} & 0.3593 \\  
			iForest & 0.3108 & 0.3422 & 0.3072 & \textbf{0.3468} \\  
			iNNE & 0.3072 & \textbf{0.3286} & 0.1724 & 0.1763 \\  
			LOF & 0.3299 & 0.3569 & \textbf{0.3829} & \textbf{0.3829} \\  
			kNN & 0.3585 & 0.3817 & 0.3855 & \textbf{0.3876} \\
			\hline  
			AVERAGE & 0.3117 & 0.3241 & 0.2982 & 0.3143 \\  
			\hline 
		\end{tabular}% 
	} 
	\caption{Average score based on the $F_1$-score $\mathcal{D}_{cd}$.}
	\label{tab:Overall F1 Score} 
\end{table}

\begin{table} [htbp]
	\resizebox{\textwidth}{!}{ 
		\begin{tabular}{|l|c|ccc|} 
			\hline 
			Detector & Cohen-Kappa-None & Cohen-Kappa-PCA & Cohen-Kappa-t-SNE & Cohen-Kappa-UMAP \\ \cline{1-5} 
			ABOD & 0.2439 & \textbf{0.2646} & 0.1577 & 0.1987 \\  
			COF & 0.2472 & \textbf{0.2581} & 0.1712 & 0.2163 \\  
			COPOD & 0.1883 & 0.1394 & 0.311 & \textbf{0.2997} \\  
			iForest & 0.2475 & 0.2812 & 0.244 & \textbf{0.2867} \\  
			iNNE & 0.2429 & \textbf{0.2662} & 0.0958 & 0.1024 \\  
			LOF & 0.268 & 0.2987 & 0.325 & \textbf{0.3264} \\  
			kNN & 0.2979 & 0.3236 & 0.3274 & \textbf{0.3282} \\ 
			\hline 
			AVERAGE & 0.248 & \textbf{0.2617} & 0.2332 & 0.2512 \\  
			\hline 
		\end{tabular}% 
	} 
	\caption{Average score based on the Cohen's Kappa score $\mathcal{D}_{cd}$.}
	\label{tab:Overall Cohen's Kappa Score} 
\end{table}	


\newpage
\subsection{Result based on $\mathcal{D}_{\neg cd}$}

For the tables in this subsection, we will only make the text bold for the DR-technique that works best compared to other DR-techniques.

\begin{table}[htbp] 
	\resizebox{\textwidth}{!}{ 
		\begin{tabular}{|l|c|ccc|} 
			\hline 
			Detector & Total-None & Total-PCA & Total-t-SNE & Total-UMAP \\   \cline{1-5} 
			ABOD & 4,4982 & \textbf{2,3612} & 1,4273 & 1,5499 \\  
			COF & 4,8427 & \textbf{2,7129} & 2,4733 & 2,2515 \\  
			COPOD & 2,4348 & 1,6367 & 1,209 & \textbf{1,6375} \\  
			iForest & 3,0817 & \textbf{2,3528} & 1,4759 & 1,5965 \\  
			iNNE & 3,0406 & \textbf{1,879} & 1,1729 & 1,1622 \\  
			LOF &  4,9196	& \textbf{2,785} &	2,551 & 2,4713 \\
			kNN & 4,9574 & \textbf{2,8712} & 1,8005 & 2,27 \\
			\hline
			AVERAGE & 3,8555 & \textbf{2,3606} & 1,7555 & 1,8764 \\
			\hline 
		\end{tabular}% 
	} 
	\caption{Average score based on the objective function} 
	\label{Overall ncd total score} 
\end{table}

\begin{table}[htbp] 
	\resizebox{\textwidth}{!}{ 
		\begin{tabular}{|l|c|ccc|} 
			\hline 
			Detector & Accuracy-None & Accuracy-PCA & Accuracy-t-SNE & Accuracy-UMAP \\   \cline{1-5} 
			ABOD & 0.9467 & \textbf{0.8768} & 0.8394 & 0.8444 \\  
			COF & 0.9585 & \textbf{0.8897} & 0.877 & 0.8699 \\  
			COPOD & 0.8751 & 0.8461 & 0.8309 & \textbf{0.8472} \\  
			iForest & 0.8981 & \textbf{0.8736} & 0.8411 & 0.8458 \\  
			iNNE & 0.8970 & \textbf{0.8565} & 0.8305 & 0.8297 \\  
			LOF & 0.9610 & \textbf{0.8917} & 0.8798 & 0.8778 \\  
			kNN & 0.9626 & \textbf{0.8946} & 0.8526 & 0.8704 \\  
			\hline
			AVERAGE & 0.9626 & \textbf{0.8756} & 0.8502 & 0.855 \\  
			\hline 
		\end{tabular}% 
	} 
	\caption{Average score based on the accuracy score $\mathcal{D}_{\neg cd}$.} 
	\label{accuracy ncd} 
\end{table}

\begin{table}[htbp] 
	\resizebox{\textwidth}{!}{ 
		\begin{tabular}{|l|c|ccc|} 
			\hline 
			Detector & Precision-None & Precision-PCA & Precision-t-SNE & Precision-UMAP \\   \cline{1-5} 
			ABOD & 0,6823 & \textbf{0,3327} & 0,1457 & 0,1707 \\  
			COF & 0,7413 & \textbf{0,3975} & 0,3338 & 0,2985 \\  
			COPOD & 0,3246 & 0,1797 & 0,1037 & \textbf{0,1847} \\  
			iForest & 0,4394 & \textbf{0,316} & 0,1533 & 0,1764 \\  
			iNNE & 0,4338 & \textbf{0,2303} & 0,1017 & 0,0958 \\  
			LOF & 0,7538 & \textbf{0,4076} & 0,348 & 0,3379 \\  
			kNN & 0,7617 & \textbf{0,4221} & 0,2118 & 0,301 \\  
			\hline
			AVERAGE & 0,591 & \textbf{0,3266} & 0,1997 & 0,2236 \\  
			\hline 
		\end{tabular}% 
	} 
	\caption{Average score based on the precision score $\mathcal{D}_{\neg cd}$.} 
	\label{precision ncd} 
\end{table}

\begin{table}[htbp] 
	\resizebox{\textwidth}{!}{ 
		\begin{tabular}{|l|c|ccc|} 
			\hline 
			Detector & MCC-None & MCC-PCA & MCC-t-SNE & MCC-UMAP \\   \cline{1-5} 
			ABOD & 0,6951 & \textbf{0,2577} & 0,0654 & 0,0906 \\  
			COF & 0,7656 & \textbf{0,33} & 0,28 & 0,2346 \\  
			COPOD & 0,272 & 0,1082 & 0,0205 & \textbf{0,1085} \\  
			iForest & 0,4047 & \textbf{0,2555} & 0,0753 & 0,1001 \\  
			iNNE & 0,3965 & \textbf{0,1582} & 0,0133 & 0,011 \\  
			LOF & 0,7814 & \textbf{0,3447} & 0,2959 & 0,2796 \\  
			kNN & 0,7891 & \textbf{0,3623} & 0,1419 & 0,2383 \\  
			\hline
			AVERAGE & 0,5863 & \textbf{0,2595} & 0,1275 & 0,1518 \\  
			\hline 
		\end{tabular}% 
	} 
	\caption{Average score based on the MCC score $\mathcal{D}_{\neg cd}$.} 
	\label{MCC ncd} 
\end{table}

\begin{table}[htbp] 
	\resizebox{\textwidth}{!}{ 
		\begin{tabular}{|l|c|ccc|} 
			\hline 
			Detector & Recall-None & Recall-PCA & Recall-t-SNE & Recall-UMAP \\   \cline{1-5} 
			ABOD & 0,7989 & \textbf{0,3257} & 0,1659 & 0,1859\\  
			COF & 0,8693 & \textbf{0,3946} & 0,3706 & 0,321 \\  
			COPOD & 0,3672 & \textbf{0,2117}	& 0,1258 & 0,2036 \\  
			iForest & 0,4987 & \textbf{0,3453} & 0,1743 & 0,1981 \\  
			iNNE & 0,4918 & \textbf{0,2513} & 0,1125 & 0,1165 \\  
			LOF & 0,8861 & \textbf{0,4136} & 0,3857 & 0,3633 \\  
			kNN & 0,8913 & \textbf{0,4312} & 0,2401 & 0,3248 \\  
			\hline
			AVERAGE & 0,6862 & \textbf{0,3391} & 0,225 & 0,2447 \\  
			\hline 
		\end{tabular}% 
	} 
	\caption{Average score based on the recall score $\mathcal{D}_{\neg cd}$.} 
	\label{recall ncd} 
\end{table}

\begin{table}[htbp] 
	\resizebox{\textwidth}{!}{ 
		\begin{tabular}{|l|c|ccc|} 
			\hline 
			Dataset & F1-Score-None & F1-Score-PCA & F1-Score-t-SNE & F1-Score-UMAP \\   \cline{1-5} 
			ABOD & 0,7014 & \textbf{0,3162} & 0,1478	& 0,17 \\  
			COF & 0,7645 & \textbf{0,3792} & 0,3381 & 0,2977 \\  
			COPOD & 0,3306 & 0,1859 & 0,1086 & \textbf{0,1869} \\  
			iForest & 0,4469 & \textbf{0,3143} & 0,1577 & 0,1785 \\  
			iNNE & 0,4377 & \textbf{0,229} & 0,1021 & 0,0995 \\  
			LOF & 0,7785 & \textbf{0,3918} & 0,3521 & 0,3382 \\  
			kNN & 0,7858 & \textbf{0,4078} & 0,2158 & 0,3015 \\  
			\hline
			AVERAGE & 0,6065 & \textbf{0,3177} & 0,2032 & 0,2246 \\  
			\hline 
		\end{tabular}% 
	} 
	\caption{Average score based on $F_1$-score $\mathcal{D}_{\neg cd}$.} 
	\label{f1 ncd} 
\end{table}

\begin{table}[htbp] 
	
	\resizebox{\textwidth}{!}{ 
		\begin{tabular}{|l|c|ccc|} 
			\hline 
			Detector & Cohen-Kappa-None & Cohen-Kappa-PCA & Cohen-Kappa-t-SNE & Cohen-Kappa-UMAP \\   \cline{1-5} 
			ABOD & 0,6739 & \textbf{0,2522} & 0,0633 & 0,0883 \\  
			COF & 0,7435 & \textbf{0,3221} & 0,2739 & 0,2299 \\  
			COPOD & 0,2654 & 0,1051	& 0,0196 & \textbf{0,1068} \\  
			iForest & 0,3939 & \textbf{0,2482} & 0,0743 & 0,0975 \\  
			iNNE & 0,3839 & \textbf{0,1537} & 0,013 & 0,0098 \\  
			LOF & 0,7588 & \textbf{0,3357} & 0,2895 & 0,2747 \\  
			kNN & 0,7671 & \textbf{0,3532} & 0,1384 & 0,234 \\  
			\hline
			AVERAGE & 0,5695 & \textbf{0,2529} & 0,1246 & 0,1487 \\  
			\hline 
		\end{tabular}% 
	} 
	\caption{Average score based on the Cohen's Kappa score $\mathcal{D}_{\neg cd}$.} 
	\label{cohen_kappa ncd} 
\end{table}

\section{Low Scores in Overall, Accuracy Score is Not Necessary}
Overall, we have low scores for the objective function as well for any other metrics in $\mathcal{D}_{cd}$. This is caused by the fact that the iteration for hyperparameter optimization is not sufficient for getting a good result. Nevertheless, we can still study the applicability of DR-techniques since we can already see some patterns emerging from the experiment result.

However, the accuracy score is relatively high compared to other metrics. The accuracy scores rarely "penalize" the precision score during the experiment. It means that the accuracy score is not necessary for measuring the quality of OD-algorithms. 

\section{High Correlation between The Metrics and The Objective Function Score}
We can derive the correlation table based on the experiment to see if we can gain more information from the objective function. Based on Table \ref{corellation on Dcd} and Table \ref{corellation on D not cd}, we can see that in general, the metric scores show a high correlation with each other and the objective function. This means that the objective function created on the hypothesis was not giving better information for optimizing the performance. The accuracy, the MCC score, and Cohen's kappa score did not help us improve the performance. The metrics that help measure the performance are $F_1$-score, precision score, and recall score. 

\begin{table} [htbp]
	\centering
	\resizebox{\textwidth}{!}{
		\begin{tabular}{|l|c|c|c|c|c|c|}
			\hline
			& Precision & Recall & $F_1$-Score & MCC & Cohen's Kappa &  Total \\ 
			\hline
			Accuracy & 0,9999 &	0,9155	& 0,9570 & 0,9982 & 0,9535 & 0,9970 \\
			Precision &   &	0,9110 & 0,9585 & 0,9973 & 0,9551 & 0,9959 \\
			Recall &  &		& 0,7744 & 0,9290 & 0,7662 & 0,9370\\
			$F_1$-Score &  & &  & 0,9523 & 0,9999 & 0,9458 \\
			MCC &  &	&  &  & 0,9484	& 0,9997\\
			Cohen's Kappa &  &		&  &  &  & 0,9415 \\
			\hline
		\end{tabular}
	}
	\caption{The correlation table between the used metric scores and the objective function in $\mathcal{D}_{cd}$.} 
	\label{corellation on Dcd} 
\end{table}

\begin{table} [htbp]
	\centering
	\resizebox{\textwidth}{!}{
		\begin{tabular}{|l|c|c|c|c|c|c|}
			\hline
			& Precision & Recall & $F_1$-Score & MCC & Cohen's Kappa &  Total \\ 
			\hline
			Accuracy &  1	& 0,9969 & 0,9991 & 0,9993 & 0,9993 & 0,9992\\
			Precision &   &	0,9970 & 0,9991	& 0,9993 & 0,9993 & 0,9993 \\
			Recall &  &		& 0,9993 & 0,9992 & 0,9992 &0,9992\\
			$F_1$-Score &  & &  & 1,0000 & 1,0000 & 1,0000 \\
			MCC &  &	&  &  & 1,0000 & 1,0000\\
			Cohen's Kappa &  &		&  &  &  & 1,0000\\
			\hline
		\end{tabular}
	}
	\caption{The correlation table between the used metric scores and the objective function in $\mathcal{D}_{\neg cd}$.} 
	\label{corellation on D not cd} 
\end{table}

\section{The Datasets in $\mathcal{D}_{\neg cd}$}
Based on the dataset in $\mathcal{D}_{\neg cd}$, we can conclude that not all data fit with the DR-technique. Using a DR-technique also leads to the loss of information. The performance of the outlier detectors decreases for any metrics. It indicates that the DR-techniques are not necessary for the given problems. The raw data could be sufficient for detecting the anomalies.

However, from $\mathcal{D}_{\neg cd}$ we can conclude that PCA could be used in most cases as the first indicator if it is worth the effort for using DR-techniques. If the performance decreases when applying PCA, it might not be worth an effort to explore other DR-techniques.

\section{Global Structure over Local Structure}
Based on the shown tables in Subsection \ref{results in Dcd}, we can see that PCA  and UMAP work best with the chosen unsupervised OD-algorithms. When we apply PCA or UMAP to the dataset in $\mathcal{D}_{cd}$, we can see there is an increase in the overall performance for each metric. It indicates that the global structure is preferred over the local structure when finding the outlier. PCA preserves the global structure since we find the hidden linear structure with the help of eigenvectors from each dimension. On the other hand, UMAP preserves the global structure under the non-linearity assumption. UMAP uses cross-entropy as the loss function, and it takes the dissimilarities between the data points into account. The loss function "encourages" the two dissimilar points to be far from each other on the embedded space. 

\section{The relation between LOF and UMAP}
Interestingly, LOF performs better when paired with UMAP. As we know, LOF focuses on finding the local outliers. It means that preserving the global structure is necessary for LOF because LOF requires sensitivity towards contextual outliers. It implies that when applying UMAP, we are indirectly performing the "clustering" method, and therefore it is easier for LOF to detect the local outlier.

\section{The relation between COPOD and t-SNE}
Although, in general, we prefer preserving the global structure when performing OD-algorithms, the COPOD algorithm seems to be contrary. COPOD performs well when paired with t-SNE. It indicates that COPOD has a strong assumption in finding the global outlier. Preserving the global structure is unnecessary since it will "give room" for COPOD to detect the local anomalies. By using t-SNE, it "hides" unnecessary local outliers. 

\section{Applicability of t-SNE in other OD-algorithms}
Though we see in general t-SNE does not help much in optimizing the OD-algorithms, in some cases, t-SNE does increase the performance of OD-algorithms besides COPOD. Table \ref{tsne useful} shows where the applicability of t-SNE has the best result compared to other DR-techniques. 

\begin{table} [htbp]
	\centering
	\resizebox{\textwidth}{!}{
		\begin{tabular}{|l|c|c|c|}
			\hline
			Dataset & OD-algorithm & $F_1$-Score - None & $F_1$-Score - t-SNE \\ 
			\hline
			Ecoli	& ABOD & 0.0870 &	0.2899 \\
			Vertebral	& ABOD & 0.0370 & 0.2963 \\
			Pendigits & COPOD &	0.0664	& 0.0783 \\
			Ecoli	& COF & 0.1449	& 0.2319 \\
			Vertebral	& COF & 0.1111	& 0.2593 \\
			Glass & iForest & 0.1290 & 0.1935 \\
			Vowels & iNNE & 0.2653 & 0.3776	\\
			Glass & kNN & 0.1935 & 0.3226 \\
			Vertebral & kNN & 0.0000 & 0.1852 \\
			Pima & LOF & 0.2551 & 0.2899 \\
			Pendigits & LOF	& 0.1186 & 0.3084 \\
			Ecoli & LOF & 0.2029 & 0.6957 \\
			Vertebral & LOF & 0.0741	& 0.2593 \\
			\hline
		\end{tabular}
	}
	\caption{Some cases where t-SNE helps increasing the performance of OD-algorithms besides COPOD. } 
	\label{tsne useful} 
\end{table}

This data implies that in the end, it is up to the practitioners to determine either preserving the global structure or not. In some cases, it is not necessary to find the local outliers. Sometimes, it is better to preserve only the local structure because it allows the OD-algorithm to focus more on finding the global outlier.

\section{ABOD may still suffer from the curse of the dimensionality}
Based on \cite{ABOD}, ABOD is an OD-algorithm that suits well with high dimensional data. However, as we can see from the experiment, ABOD can still be supported by DR-techniques, mostly with PCA. It means that the ABOF score may still depend on the distance between points. The measurement takes the distance between points as the weight for each created angle. The curse of dimensionality can distort the weight in ABOF. Therefore, using DR-techniques, one can minimize this problem.

