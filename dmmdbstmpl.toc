\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Introduction}{3}{}%
\contentsline {chapter}{\numberline {2}Fundamentals and Related Works}{6}{}%
\contentsline {section}{\numberline {2.1}Related Works}{6}{}%
\contentsline {section}{\numberline {2.2}The Outlier Detection Algorithms}{7}{}%
\contentsline {subsection}{\numberline {2.2.1}LOF}{7}{}%
\contentsline {paragraph}{$k$-distance Concept}{7}{}%
\contentsline {paragraph}{Reachability Distance}{7}{}%
\contentsline {paragraph}{Local reachability density}{7}{}%
\contentsline {paragraph}{LOF score}{8}{}%
\contentsline {subsection}{\numberline {2.2.2}COF}{8}{}%
\contentsline {paragraph}{Nearest Neighbor Concept}{9}{}%
\contentsline {paragraph}{Set Based Nearest Path (SBN-path)}{9}{}%
\contentsline {paragraph}{Set Based Nearest Trail {SBN-trail}}{9}{}%
\contentsline {paragraph}{Average Chaining Distance}{10}{}%
\contentsline {paragraph}{COF Score}{10}{}%
\contentsline {subsection}{\numberline {2.2.3}kNN}{10}{}%
\contentsline {paragraph}{$k$-nearest neighbor distance}{11}{}%
\contentsline {paragraph}{Outlier Score based on k-nearest neighbor}{11}{}%
\contentsline {subsection}{\numberline {2.2.4}ABOD}{11}{}%
\contentsline {paragraph}{General Idea}{12}{}%
\contentsline {paragraph}{ABOF Score}{12}{}%
\contentsline {paragraph}{Approximate ABOF for FastABOD}{13}{}%
\contentsline {subsection}{\numberline {2.2.5}COPOD}{13}{}%
\contentsline {paragraph}{Copula Models}{13}{}%
\contentsline {paragraph}{Empirical Copula}{13}{}%
\contentsline {paragraph}{Left and Right Tail Probabilities}{14}{}%
\contentsline {paragraph}{Skewness Correction}{14}{}%
\contentsline {paragraph}{Minimize Tail Probabilities}{15}{}%
\contentsline {paragraph}{Outlier Score}{15}{}%
\contentsline {subsection}{\numberline {2.2.6}Isolation Forest}{16}{}%
\contentsline {paragraph}{Isolation Tree (iTree)}{16}{}%
\contentsline {paragraph}{Path Length}{16}{}%
\contentsline {paragraph}{Defining outlier score}{17}{}%
\contentsline {subsection}{\numberline {2.2.7}iNNE}{17}{}%
\contentsline {paragraph}{Hypersphere Concept}{17}{}%
\contentsline {paragraph}{Isolation Score}{17}{}%
\contentsline {paragraph}{Anomaly Score}{18}{}%
\contentsline {section}{\numberline {2.3}The Dimensionality Reduction Techniques}{19}{}%
\contentsline {subsection}{\numberline {2.3.1}PCA}{19}{}%
\contentsline {paragraph}{Covariance Matrix}{19}{}%
\contentsline {paragraph}{Eigenvalues $\lambda $}{20}{}%
\contentsline {paragraph}{Eigenvectors $v$}{20}{}%
\contentsline {paragraph}{Finding reduced dimension}{20}{}%
\contentsline {subsection}{\numberline {2.3.2}t-SNE}{20}{}%
\contentsline {paragraph}{Stochastic Neighbor Embedding}{20}{}%
\contentsline {paragraph}{t-SNE}{21}{}%
\contentsline {subsection}{\numberline {2.3.3}UMAP}{21}{}%
\contentsline {paragraph}{$k$-neighbor graph}{22}{}%
\contentsline {paragraph}{Cross Entropy}{23}{}%
\contentsline {section}{\numberline {2.4}Tree-structured Parzen Estimator Approach (TPE)}{23}{}%
\contentsline {paragraph}{Hyperparameter Optimization Function}{24}{}%
\contentsline {paragraph}{Expected Improvement Function}{24}{}%
\contentsline {paragraph}{Bayesian Optimization}{24}{}%
\contentsline {chapter}{\numberline {3}Measurement}{26}{}%
\contentsline {section}{\numberline {3.1}Accuracy}{27}{}%
\contentsline {section}{\numberline {3.2}Precision}{27}{}%
\contentsline {section}{\numberline {3.3}Recall}{28}{}%
\contentsline {section}{\numberline {3.4}$F_{1}$-Score}{28}{}%
\contentsline {section}{\numberline {3.5}Matthews Correlation Coefficient (MCC)}{28}{}%
\contentsline {section}{\numberline {3.6}Cohen's Kappa Coefficient ($\kappa $)}{29}{}%
\contentsline {chapter}{\numberline {4}Experiment}{30}{}%
\contentsline {section}{\numberline {4.1}Objective Function}{31}{}%
\contentsline {section}{\numberline {4.2}Experiment Result}{33}{}%
\contentsline {subsection}{\numberline {4.2.1}Result based on $\mathcal {D}_{cd}$}{33}{}%
\contentsline {subsection}{\numberline {4.2.2}Result based on $\mathcal {D}_{\neg cd}$}{36}{}%
\contentsline {section}{\numberline {4.3}Low Scores in Overall, Accuracy Score is Not Necessary}{36}{}%
\contentsline {section}{\numberline {4.4}High Correlation between The Metrics and The Objective Function Score}{38}{}%
\contentsline {section}{\numberline {4.5}The Datasets in $\mathcal {D}_{\neg cd}$}{38}{}%
\contentsline {section}{\numberline {4.6}Global Structure over Local Structure}{39}{}%
\contentsline {section}{\numberline {4.7}The relation between LOF and UMAP}{40}{}%
\contentsline {section}{\numberline {4.8}The relation between COPOD and t-SNE}{40}{}%
\contentsline {section}{\numberline {4.9}Applicability of t-SNE in other OD-algorithms}{40}{}%
\contentsline {section}{\numberline {4.10}ABOD may still suffer from the curse of the dimensionality}{40}{}%
\contentsline {chapter}{\numberline {5}Conclusion and Further Studies}{42}{}%
\contentsline {chapter}{Bibliography}{44}{}%
\contentsline {chapter}{\numberline {A}Experiment}{48}{}%
\contentsline {section}{\numberline {A.1}Experiment Result}{48}{}%
\contentsline {section}{\numberline {A.2}Hyperparameter Tuning}{59}{}%
\contentsline {subsection}{\numberline {A.2.1}\tt {n\_components}}{59}{}%
