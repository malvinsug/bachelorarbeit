import pandas as pd
import os

if __name__ == "__main__":
    directory_name = "/home/muffinsea/Documents/bachelorarbeit/radar_chart_maker/dataset/"
    filenames = os.listdir(directory_name)
    filenames.sort()
    empty = ""
    xlsx = ".xlsx"

    for filename in filenames:
        df = pd.read_excel(directory_name+filename, sheet_name=None)
        for key in df.items():
            print(f"creating {filename.replace(xlsx,empty)}_{key[0]}.csv")
            df[key[0]].to_csv(f"csv/{filename.replace(xlsx,empty)}_{key[0]}.csv",index=False)